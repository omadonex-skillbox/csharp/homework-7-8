﻿using HomeWork_7_8;

internal class Program
{
    const string DataPath = "data.txt";          
    static int[] modeList = { 0, 1, 2, 3, 4, 5 };

    private static Repository repository;
    private static void Main(string[] args)
    {
        int mode;
        repository = new Repository(DataPath);

        do
        {
            Console.WriteLine("");
            Console.WriteLine("==================================");
            Console.WriteLine("Input option variants:");
            Console.WriteLine("1 - Get all workers");
            Console.WriteLine("2 - Get worker by id");
            Console.WriteLine("3 - Get workers between dates");
            Console.WriteLine("4 - Add new worker");
            Console.WriteLine("5 - Delete worker by id");
            Console.WriteLine("0 - Exit");
            Console.WriteLine("==================================");
            Console.WriteLine("");
            if (!int.TryParse(Console.ReadLine(), out mode) || Array.IndexOf(modeList, mode) == -1)
            {
                Console.WriteLine("Incorrect option. Available options: [0,1,2,3,4,5]");
            }
            else
            {
                switch (mode)
                {
                    case 1:
                        readAllMode();
                        break;
                    case 2:
                        getByIdMode();
                        break;
                    case 3:
                        getBetweenDatesMode();
                        break;
                    case 4:
                        addNewMode();
                        break;
                    case 5:
                        deleteByIdMode();
                        break;
                }
            }
        } while (mode != 0);
    }

    private static void readAllMode()
    {
        Console.WriteLine("");
        Console.WriteLine("##### Getting dictionary data #####");

        Worker[] data = repository.GetAllWorkers();
        foreach (Worker worker in data)
        {
            worker.Print();
        }                  
    }

    private static void getByIdMode()
    {
        Console.WriteLine("");
        Console.WriteLine("##### Getting concrete worker #####");
        Console.WriteLine("Input worker id:");
        int id = int.Parse(Console.ReadLine());

        try
        {
            Console.WriteLine("Search result:");
            Worker worker = repository.GetWorkerById(id);
            worker.Print();
        } catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }        
    }

    private static void getBetweenDatesMode()
    {
        Console.WriteLine("");
        Console.WriteLine("##### Getting workers between dates #####");
        Console.WriteLine("Input date from:");
        DateTime from = DateTime.Parse(Console.ReadLine());
        Console.WriteLine("Input date to:");
        DateTime to = DateTime.Parse(Console.ReadLine());

        try
        {
            Console.WriteLine("Search result:");
            Worker[] workers = repository.GetWorkersBetweenDates(from, to);
            foreach (Worker worker in workers)
            {
                worker.Print();
            }            
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }

    private static void addNewMode()
    {
        Console.WriteLine("");
        Console.WriteLine("##### Input worker data #####");       

        string[] data = new string[Worker.Fields.Length];
        int i = 0;
        foreach (string field in Worker.Fields)
        {
            Console.WriteLine($"input '{field}'");
            data[i] = Console.ReadLine();
            i++;
        }
        
        repository.AddWorker(new Worker(data));       

        Console.WriteLine("Data successfully added");
    }

    private static void deleteByIdMode()
    {
        Console.WriteLine("");
        Console.WriteLine("##### Deleting concrete worker #####");
        Console.WriteLine("Input worker id:");
        int id = int.Parse(Console.ReadLine());   
        repository.DeleteById(id);
        Console.WriteLine("Worker successfully deleted");
    }
}