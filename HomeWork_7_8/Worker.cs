﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork_7_8
{
    struct Worker
    {
        private const char SerializeDelimiter = '#';

        public static string[] Fields = { "date", "name", "age", "height", "date_birth", "place_birth" };
        public int Id {  get; set; }
        public DateTime Date { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public int Height { get; set; }
        public DateTime DateBirth { get; set; }
        public string PlaceBirth { get; set; }

        public Worker(string[] fieldsData)
        {
            this.Id = 0;
            this.Date = DateTime.Parse(fieldsData[0]);
            this.Name = fieldsData[1];
            this.Age = int.Parse(fieldsData[2]);
            this.Height = int.Parse(fieldsData[3]);
            this.DateBirth = DateTime.Parse(fieldsData[4]);
            this.PlaceBirth = fieldsData[5];
        }

        public Worker(string serializedData)
        {
            string[] parsedData = serializedData.Split(SerializeDelimiter);

            this.Id = int.Parse(parsedData[0]);
            this.Date = DateTime.Parse(parsedData[1]);
            this.Name = parsedData[2];
            this.Age = int.Parse(parsedData[3]);
            this.Height = int.Parse(parsedData[4]);
            this.DateBirth = DateTime.Parse(parsedData[5]);
            this.PlaceBirth = parsedData[6];
        }

        public string Serialize()
        {
            char D = SerializeDelimiter;

            return $"{Id}{D}{Date}{D}{Name}{D}{Age}{D}{Height}{D}{DateBirth}{D}{PlaceBirth}";
        }

        public void Print()
        {
            Console.WriteLine($"Record #{Id}");
            Console.WriteLine($"Date: {Date}");
            Console.WriteLine($"Name: {Name}");
            Console.WriteLine($"Age: {Age}");
            Console.WriteLine($"Height: {Height}");
            Console.WriteLine($"DateBirth: {DateBirth}");
            Console.WriteLine($"PlaceBirth: {PlaceBirth}");
            Console.WriteLine("");
        }
    }
}
