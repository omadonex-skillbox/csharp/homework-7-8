﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork_7_8
{
    internal class Repository
    {
        public const char DataDelimiter = '#';
        private string path;

        public Repository(string path) 
        {
            this.path = path;
        }

        public Worker[] GetAllWorkers()
        {
            return load();
        }

        public Worker GetWorkerById(int id) 
        {
            Worker[] data = GetAllWorkers();
            Worker[] filtered = data.Where(worker => worker.Id == id).ToArray();

            if (filtered.Length > 1) 
            {
                throw new Exception("Error in database. Founded more than one record with same ID");                                
            }

            if (filtered.Length == 0)
            {
                throw new Exception("Worker not found");
            }

            return filtered[0];
        }

        public Worker[] GetWorkersBetweenDates(DateTime from, DateTime to)
        {
            Worker[] data = GetAllWorkers();
            Worker[] filtered = data.Where(worker => worker.Date >= from && worker.Date <= to).ToArray();

            if (filtered.Length == 0)
            {
                throw new Exception("Workers not found");
            }

            return filtered;
        }

        private Worker[] load()
        {
            if (File.Exists(path))
            { 
                string[] lines = File.ReadAllLines(path);
                Worker[] data = new Worker[lines.Length];
                int index = 0;
                foreach (string line in lines)
                {                            
                    data[index] = new Worker(line);
                    index++;
                }

                return data;
            }
            
            Console.WriteLine("Dictionary not exists!");

            return new Worker[0];                        
        }

        private int getNextId()
        {
            Worker[] data = GetAllWorkers();
            int maxId = 0;
            foreach (Worker worker in data)
            {
                if (worker.Id > maxId)
                {
                    maxId = worker.Id;
                }
            }

            return maxId + 1;
        }

        public void AddWorker(Worker worker)
        {
            worker.Id = getNextId();

            string[] lines = new string[1];
            lines[0] = worker.Serialize();
            File.AppendAllLines(path, lines);
        }

        public void DeleteById(int id)
        {
            Worker[] data = GetAllWorkers();
            Worker[] filtered = data.Where(worker => worker.Id != id).ToArray();
            string[] lines = new string[filtered.Length];
            int index = 0;
            foreach (Worker worker in filtered)
            {
                lines[0] = worker.Serialize();
                index++;
            }

            File.WriteAllLines(path, lines);            
        }
    }
}
